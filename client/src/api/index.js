import {create} from "apisauce";
import i18n from 'i18next';

import * as stores from "./../stores";

const api = create({
  baseURL: "http://localhost:3000",
  header: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
});

api.addRequestTransform(request => {
  request.headers['x-access-token'] = stores.authStore.token;
});

api.addResponseTransform(response => {
  const {ok, problem, data} = response;
  if (!ok) {
    switch (problem) {
      case 'CLIENT_ERROR':
        data.errorMessage = i18n.t("error.400");
        if (data.status === 401) {
          stores.authStore.accessTokenExpired();
        }
        break;
      case 'SERVER_ERROR':
        data.errorMessage = i18n.t("error.500");
        break;
      case 'TIMEOUT_ERROR':
        data.errorMessage = i18n.t("error.timeout");
        break;
      case 'CONNECTION_ERROR':
        data.errorMessage = i18n.t("error.connection");
        break;
      case 'NETWORK_ERROR':
        data.errorMessage = i18n.t("error.network");
        break;
      case 'CANCEL_ERROR':
        break;
      default:
        break;
    }
  }
});

export default api;