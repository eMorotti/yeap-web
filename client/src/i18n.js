import i18n from 'i18next';
import Backend from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import {reactI18nextModule} from 'react-i18next';
import {observable} from 'mobx';

i18n.use(Backend)
  .use(LanguageDetector)
  .use(reactI18nextModule)
  .init({
    fallbackLng: "en",

    ns: ["translation"],
    defaultNS: "translation",

    debug: true,

    interpolation: {
      escapeValue: false
    },

    react: {
      defaultTransParent: "div",
      wait: true
    }
  });

// Rende i18n.t osservabile nei coputed degli store
const currentLanguage = observable(i18n.language);

i18n.on("languageChanged", (lng) => {
  currentLanguage.set(lng);
});

const wrappedT = i18n.t;
i18n.t = (...args) => {
  currentLanguage.get();
  return wrappedT.call(i18n, ...args);
};

export default i18n;