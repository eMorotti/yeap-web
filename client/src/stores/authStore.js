import {types, flow, applySnapshot, getSnapshot} from "mobx-state-tree";
import {reaction} from "mobx";

import api from "./../api";
import {errorStore} from "./";

const DEFAULT_COUNTRYCODE = "+1";
const DEFAULT_PHONE = "4126504403";
const DEVICE_ID = "WEB";

function accountKit_Init() {
  if (!window.AccountKit) {
    (callback => {
      const tag = document.createElement("script");
      tag.setAttribute(
        "src",
        `https://sdk.accountkit.com/en_US/sdk.js`
      );
      tag.setAttribute("id", "account-kit");
      tag.setAttribute("type", "text/javascript");
      tag.onload = callback;
      document.head.appendChild(tag);
    })(() => {
      window.AccountKit_OnInteractive = _accountKit_Init;
    });
  }
}

function _accountKit_Init() {
  window.AccountKit.init(
    {
      appId: process.env.REACT_APP_ACCOUNT_KIT_KEY,
      state: process.env.REACT_APP_ACCOUNT_KIT_STATE,
      version: "v2.12",
      fbAppEventsEnabled: true
    }
  );
}

export default types
  .model("AuthStore", {
    accessToken: types.optional(types.string, ""),
    user: types.frozen,
    state: types.maybe(types.enumeration("State", ["pending", "done", "error"]))
  })
  .views(self => ({
    get token() {
      return self.accessToken;
    },

    get isLoggedIn() {
      return self.accessToken && self.accessToken !== "null";
    },

    get userUUID() {
      return self.user.uuid;
    }
  }))
  .actions(self => {
    function runAccountKitSmsLogin({countryCode, phoneNumber}) {
      return new Promise((resolve, reject) => {
        window.AccountKit.login(
          'PHONE',
          {countryCode, phoneNumber},
          (response) => {
            resolve(response);
          }
        )
      });
    }

    const afterCreate = () => {
      accountKit_Init();

      if (typeof window !== undefined && window.localStorage) {
        const authInfo = window.localStorage.getItem("authInfo");
        if (authInfo) {
          applySnapshot(self, JSON.parse(authInfo));
        }

        reaction(
          () => getSnapshot(self),
          json => {
            window.localStorage.setItem("authInfo", JSON.stringify(json))
          }
        );
      }
    };

    const accountKit_SmsLogin = flow(function*() {
      const response = yield runAccountKitSmsLogin({countryCode: DEFAULT_COUNTRYCODE, phoneNumber: DEFAULT_PHONE});

      if (response.status === "PARTIALLY_AUTHENTICATED") {
        var code = response.code;
        // var csrf = response.state;
        // Send code to server to exchange for access token
        const {ok, data} = yield api.post("/api/account/login", {
          authorizationCode: code,
          deviceId: DEVICE_ID
        });
        if (ok) {
          self.accessToken = data.accessToken;
          self.user = data.profile;
        } else {
          errorStore.notifyError(data.errorMessage);
        }
      }
      else if (response.status === "NOT_AUTHENTICATED") {
        //   handle authentication failure
        errorStore.notifyError("Errore di autenticazione");
      }
      else if (response.status === "BAD_PARAMS") {
        //   handle bad parameters
        errorStore.notifyError("Errore");
      }
    });

    function accessTokenExpired() {
      self.accessToken = "";
    }

    function logout() {
      self.accessToken = "";
    }

    return {
      afterCreate,
      accountKit_SmsLogin,
      accessTokenExpired,
      logout
    }
  })
  .create({});