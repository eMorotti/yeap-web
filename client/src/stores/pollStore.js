import {types, flow} from "mobx-state-tree";

import api from "./../api";
import {errorStore} from "./";

export default types.model("PollStore", {
  question: types.optional(types.string, ""),
  entryList: types.optional(types.map(types.string), {}),
  entry: types.optional(types.string, ""),
  state: types.maybe(types.enumeration("State", ["pending", "done", "error"]))
}).views(self => ({
  getEntryFromList(id) {
    return self.entryList[id];
  }
})).actions(self => {
  function setQuestion(question) {
    self.question = question;
  }

  function setEntryInList(id, entry) {
    self.entryList[id] = entry;
  }

  function setEntry(entry) {
    self.entry = entry;
  }

  const submitEntry = flow(function*(activityUuid, pollUuid) {
    self.state = "pending";
    const {ok, data} = yield api.post(`api/activity/${activityUuid}/polls/${pollUuid}/item`, {
      what: self.entry
    });
    self.entry = "";
    if (!ok) {
      self.state = "error";
      errorStore.notifyError(data.errorMessage);
    } else {
      self.state = "done";
    }
  });

  const submitPoll = flow(function*(activityUuid) {
  });

  return {
    setQuestion,
    setEntryInList,
    setEntry,
    submitEntry,
    submitPoll
  }
}).create({});