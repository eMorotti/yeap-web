import {types, flow} from "mobx-state-tree";
import {geocodeByPlaceId, getLatLng} from 'react-places-autocomplete'

import api from "./../../api";
import {errorStore} from "./../";
import * as dateUtils from "./../../utils/dateUtils";

export default types.model("CraetePollItemStore", {
  activityUuid: types.string,
  pollUuid: types.string,
  entry: types.optional(types.string, ""),
  fromDate: types.optional(types.string, ""),
  fromHour: types.optional(types.string, ""),
  toDate: types.optional(types.string, ""),
  toHour: types.optional(types.string, ""),
  where: types.optional(types.string, ""),
  placeId: types.optional(types.string, ""),
  state: types.maybe(types.enumeration("State", ["pending", "done", "error"]))
}).views(self => ({
  get isLoading() {
    return self.state === "pending";
  }
})).actions(self => {
  function setEntry(entry) {
    self.entry = entry;
  }

  function setFromDate(fromDate) {
    if (fromDate) {
      if (!isNaN(parseInt(fromDate.slice(-1), 10))) {
        if (fromDate.length === 2 || fromDate.length === 5) {
          fromDate += "/";
        }
        self.fromDate = fromDate;
      }
    } else {
      self.fromDate = fromDate;
    }
  }

  function setFromHour(fromHour) {
    if (fromHour) {
      if (!isNaN(parseInt(fromHour.slice(-1), 10))) {
        if (fromHour.length === 2) {
          fromHour += ":";
        }
        self.fromHour = fromHour;
      }
    } else {
      self.fromHour = fromHour;
    }
  }

  function setToDate(toDate) {
    if (toDate) {
      if (!isNaN(parseInt(toDate.slice(-1), 10))) {
        if (toDate.length === 2 || toDate.length === 5) {
          toDate += "/";
        }
        self.toDate = toDate;
      }
    } else {
      self.toDate = toDate;
    }
  }

  function setToHour(toHour) {
    if (toHour) {
      if (!isNaN(parseInt(toHour.slice(-1), 10))) {
        if (toHour.length === 2) {
          toHour += ":";
        }
        self.toHour = toHour;
      }
    } else {
      self.toHour = toHour;
    }
  }

  function setWhere(where) {
    self.where = where;
    self.placeId = "";
  }

  function setPlace(where, placeId) {
    self.where = where;
    self.placeId = placeId;
  }

  function resetValues() {
    self.entry = "";
    self.fromDate = "";
    self.fromHour = "";
    self.toDate = "";
    self.toHour = "";
    self.where = "";
    self.placeId = "";
  }

  const submitPollItem = flow(function*() {
    self.state = "pending";
    const place = yield retrievePLaceInfo();
    const dates = formatDates();
    const {ok, data} = yield api.post(`api/activity/${self.activityUuid}/polls/${self.pollUuid}/item`, {
      what: self.entry || undefined,
      ...dates,
      ...place
    });
    resetValues();
    if (!ok) {
      self.state = "error";
      errorStore.notifyError(data.errorMessage);
    } else {
      self.state = "done";
    }
  });

  function formatDates() {
    let fromDate = self.fromDate || undefined;
    if (fromDate) {
      const fromDate_date = new Date(fromDate);
      if (fromDate_date instanceof Date && !isNaN(fromDate_date.valueOf())) {
        fromDate = dateUtils.formatDate(fromDate_date);
      }
    }
    let toDate = self.toDate || undefined;
    if (toDate) {
      const toDate_date = new Date(toDate);
      if (toDate_date instanceof Date && !isNaN(toDate_date.valueOf())) {
        toDate = dateUtils.formatDate(toDate_date);
      }
    }
    return {
      fromDate,
      fromHour: self.fromHour || undefined,
      toDate,
      toHour: self.toHour || undefined
    };
  }

  const retrievePLaceInfo = flow(function*() {
    let location = undefined;
    if (self.where) {
      if (self.placeId) {
        try {
          const result = yield geocodeByPlaceId(self.placeId);
          if (result) {
            const {lat, lng} = yield getLatLng(result[0]);
            location = {
              lat,
              lng
            };
          }
        } catch (ex) {
          console.log(ex);
        }
      }
    }

    return {
      where: self.where || undefined,
      placeId: self.placeId || undefined,
      lat: location ? location.lat : undefined,
      lng: location ? location.lng : undefined
    }
  });

  return {
    setEntry,
    setFromDate,
    setFromHour,
    setToDate,
    setToHour,
    setWhere,
    setPlace,
    submitPollItem
  }
});