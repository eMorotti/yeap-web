import {types, flow} from "mobx-state-tree";

import api from "./../../api";
import {errorStore} from "./../";

export default types.model("CreatePollStore", {
  activityUuid: types.string,
  question: types.optional(types.string, ""),
  entryList: types.optional(types.array(types.string), []),
  state: types.maybe(types.enumeration("State", ["pending", "done", "error"]))
}).views(self => ({
  getEntryFromList(index) {
    return (self.entryList.length > index && self.entryList[index] !== undefined) || "";
  },

  get isLoading() {
    return self.state === "pending";
  }
})).actions(self => {
  function setQuestion(question) {
    self.question = question;
  }

  function setEntryInList(index, entry) {
    if (self.entryList.length > index && self.entryList[index] !== undefined) {
      self.entryList[index] = entry;
    } else {
      self.entryList.push(entry);
    }
  }

  const submitPoll = flow(function*() {
    self.state = "pending";
    const {ok, data} = yield api.post(`api/activity/${self.activityUuid}/poll`, {
      question: self.question,
      pollItems: self.entryList.map(entry => {
        return {what: entry}
      })
    });
    self.question = "";
    self.entryList = [];
    if (!ok) {
      self.state = "error";
      errorStore.notifyError(data.errorMessage);
    } else {
      self.state = "done";
    }
  });

  return {
    setQuestion,
    setEntryInList,
    submitPoll
  }
});