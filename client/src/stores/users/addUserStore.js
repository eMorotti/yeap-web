import {types, flow} from "mobx-state-tree";

import api from "./../../api";
import {errorStore} from "./../";

export default types.model("AddUserStore", {
  activityUuid: types.string,
  phone: types.optional(types.string, ""),
  state: types.maybe(types.enumeration("State", ["pending", "done", "error"]))
}).views(self => ({
  get isLoading() {
    return self.state === "pending";
  }
})).actions(self => {
  function setPhone(phone) {
    if (phone) {
      if (!isNaN(parseInt(phone.slice(-1), 10))) {
        if (phone.length === 1 && phone !== "+") {
          phone = "+" + phone;
        }
        self.phone = phone;
      } else if(phone === "+") {
        self.phone = "";
      }
    } else {
      self.phone = phone;
    }
  }

  const submitInvite = flow(function*() {
    self.state = "pending";
    const {ok, data} = yield api.post(`api/activity/${self.activityUuid}/add-partecipant`, {
      phone: self.phone,
    });
    self.phone = "";
    if (!ok) {
      self.state = "error";
      errorStore.notifyError(data.errorMessage);
    } else {
      self.state = "done";
    }
  });

  return {
    setPhone,
    submitInvite
  }
});