import {types, flow} from "mobx-state-tree";
import api from "./../api";

export default types
  .model("ProfileStore", {
    nickname: types.optional(types.string, ""),
    fileName: types.optional(types.string, ""),
    file: types.frozen,
    user: types.frozen
  })
  .actions(self => {
    function setAvatar(file) {
      self.file = file;
      self.fileName = file.name;
    }

    function setNickname(nickname) {
      self.nickname = nickname;
    }

    const onSubmit = flow(function*() {
      if(!self.nickname) {
        return;
      }

      var uploadRef = null;

      if (self.file) {
        const formdata = new FormData();
        formdata.append("image", self.file);

        const {ok, data} = yield api.post("/api/upload/avatar", formdata);
        if (ok) {
          uploadRef = data.uploadref;
        }
      }

      const {ok, data} = yield api.post("/api/account/profile", {
        nickname: self.nickname,
        avatar: uploadRef
      });
      if (ok) {
        self.user = data;
      }
    });

    return {
      setAvatar,
      setNickname,
      onSubmit
    }
  })
  .create({});