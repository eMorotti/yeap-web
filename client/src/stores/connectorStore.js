import {types} from "mobx-state-tree";
import io from 'socket.io-client';
import {authStore, modelStore} from "./";

let socket;
let socketCounter = 0;

function openSocket() {
  socket = io('/', {
    query: 'token=' + encodeURIComponent(authStore.token) + '&clientVersion=2',
    transports: ['websocket']
  });

  socket.on("connect", () => {
    console.log('socket connect', socket.id);
  });
  socket.on('error', (err) => {
    console.log('socket error', err);
  });
  socket.on('connect_error', (err) => {
    console.log('socket connect_error', err);
  });
  socket.on('disconnect', () => {
    console.log('socket disconnect');
  });
  socket.on('message', (message) => {
    console.log('socket message', message);
    onMessageReceived(message);
  })
}

function onMessageReceived(message) {
  if (message) {
    if (message.id) {
      socket.emit("ack", message.id);
      if (message.body) {
        const {data} = message.body;
        if (data) {
          if(data.activityUuid) {
            modelStore.refreshActivity(data.activityUuid);
          } else  {
            modelStore.loadActivities();
          }
        }
      }
    }
  }
}

export default types.model("Connector")
  .actions(self => {
    function start() {
      if (socketCounter === 0) {
        openSocket();
      }
      socketCounter++;
    }

    function stop() {
      socketCounter--;
      if (socketCounter === 0) {
        socket.close();
      }
    }

    return {
      start,
      stop
    }
  }).create({});