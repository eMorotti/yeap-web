import {types, flow, applySnapshot, getSnapshot, getParent} from "mobx-state-tree";
import {reaction} from "mobx";
import i18n from 'i18next';

import api from "./../api";
import {authStore, errorStore} from "./";
import CreatePollItemStore from "./polls/createPollItemStore";
import CreatePollStore from "./polls/createPollStore";
import AddUserStore from "./users/addUserStore";

const Vote = types.model("Vote", {
  createdAt: types.number,
  type: types.number,
  userUuid: types.string
}).views(self => ({
  get isInteresse() {
    return self.type === 3 || self.type === 4
  },

  get isInteresseSi() {
    return self.type === 3
  },

  get isInteresseNo() {
    return self.type === 4
  },

  get isConferma() {
    return self.type === 1 || self.type === 2
  },

  get isConfermaSi() {
    return self.type === 1
  },

  get isConfermaNo() {
    return self.type === 2
  },
}));


const Event = types.model("Event", {
  createdAt: types.number,
  fromDate: types.maybe(types.string),
  fromHour: types.maybe(types.string),
  toDate: types.maybe(types.string),
  toHour: types.maybe(types.string),
  what: types.string,
  lng: types.maybe(types.number),
  lat: types.maybe(types.number),
  placeId: types.maybe(types.string),
  where: types.maybe(types.string),
  votes: types.optional(types.array(Vote), [])
}).views(self => ({
  get activity() {
    return getParent(self);
  },

  get numInteressati() {
    return self.votes.filter(vote => vote.isInteresseSi).length;
  },

  get numConfermati() {
    return self.votes.filter(vote => vote.isConfermaSi).length;
  },

  get interessati() {
    return self.votes.filter(vote => vote.isInteresseSi);
  },

  get nonInteressati() {
    return self.votes.filter(vote => vote.isInteresseNo);
  },

  get confermati() {
    return self.votes.filter(vote => vote.isConfermaSi);
  },

  get nonConfermati() {
    return self.votes.filter(vote => vote.isConfermaNo);
  },

  get votesTitle() {
    if(self.activity.status === 1){
      return i18n.t("activity.event.vote.interest.title");
    } else {
      return i18n.t("activity.event.vote.confirm.title");
    }
  },

  get votesYes() {
    if(self.activity.status === 1){
      return self.interessati;
    } else {
      return self.confermati;
    }
  },

  get votesNo() {
    if(self.activity.status === 1){
      return self.nonInteressati;
    } else {
      return self.nonConfermati;
    }
  },

  get votesNotVotedText() {
    if(self.activity.status === 1){
      const nonVotatoInteresse = self.activity.partecipants.length - self.interessati.length - self.nonInteressati.length;
      if (nonVotatoInteresse === 0) {
        return i18n.t("activity.event.vote.all_voted");
      }
      return i18n.t("activity.event.vote.not_voted", {count: nonVotatoInteresse});
    } else {
      const nonVotatoConferma = self.activity.partecipants.length - self.confermati.length - self.nonConfermati.length;
      if (nonVotatoConferma === 0) {
        return i18n.t("activity.event.vote.all_voted");
      }
      return i18n.t("activity.event.vote.not_voted", {count: nonVotatoConferma});
    }
  }
}));


const PollItem = types.model("PollItem", {
  createdAt: types.number,
  creatorUuid: types.string,
  fromDate: types.maybe(types.string),
  fromHour: types.maybe(types.string),
  toDate: types.maybe(types.string),
  toHour: types.maybe(types.string),
  what: types.maybe(types.string),
  lng: types.maybe(types.number),
  lat: types.maybe(types.number),
  placeId: types.maybe(types.string),
  where: types.maybe(types.string),
  uuid: types.string,
  votes: types.optional(types.array(Vote), []),
  deleted: types.optional(types.boolean, false),
  state: types.maybe(types.enumeration("State", ["pending", "done", "error"])),
}).views(self => ({
  get poll() {
    return getParent(self, 2);
  },

  get isLoading() {
    return self.state === "pending";
  },

  get voce() {
    const quando = [self.fromDate, self.fromHour, self.toDate, self.toHour].join(" ");
    return self.what || self.where || quando;
  },

  get mioVoto() {
    return self.votes.find(vote => vote.userUuid === authStore.userUUID && vote.type === 1);
  },

  get canRemoveItem() {
    return self.poll.activity.imAdmin || self.creatorUuid === authStore.userUUID;
  }
})).actions(self => {
  const vote = flow(function*() {
    if (self.state === "pending") {
      return;
    }

    self.state = "pending";
    const activityUuid = self.poll.activity.uuid;
    const pollUuid = self.poll.uuid;
    const pollItemUuid = self.uuid;
    const {type} = self.votes.find(vote => vote.userUuid === authStore.userUUID) || {};
    const {ok, data} = yield api.post(`api/activity/${activityUuid}/polls/${pollUuid}/items/${pollItemUuid}/vote`, {
      type: type === 1 ? 2 : 1
    });

    if (!ok) {
      self.state = "error";
      errorStore.notifyError(data.errorMessage);
    } else {
      self.state = "done";
    }
  });

  const removeItem = flow(function*() {
    if (self.state === "pending") {
      return;
    }

    self.state = "pending";
    const {ok, data} = yield api.delete(`api/activity/${self.poll.activity.uuid}/polls/${self.poll.uuid}/items/${self.uuid}`);
    if (!ok) {
      self.state = "error";
      errorStore.notifyError(data.errorMessage);
    } else {
      self.state = "done";
    }
  });

  return {
    vote,
    removeItem
  }
});


const Poll = types.model("Poll", {
  createdAt: types.number,
  creatorUuid: types.string,
  pollItems: types.optional(types.array(PollItem), []),
  question: types.string,
  type: types.number,
  usersCanAddItems: types.boolean,
  uuid: types.string
}).views(self => ({
  get activity() {
    return getParent(self, 2);
  },

  get pollItemsActive() {
    return self.pollItems.filter(pollItem => !pollItem.deleted);
  },

  get isDove() {
    return self.type === 2;
  },

  get isQuando() {
    return self.type === 3;
  },

  get canAddItems() {
    return self.usersCanAddItems || self.activity.imAdmin;
  }
})).actions(self => {
  function createPollItemStore() {
    return CreatePollItemStore.create({activityUuid: self.activity.uuid, pollUuid: self.uuid});
  }

  return {
    createPollItemStore
  }
});

const User = types.model("User", {
  nickname: types.string,
  phone: types.string,
  uuid: types.string
}).views(self => ({
  get activity() {
    return getParent(self, 2);
  },

  get avatarSmall() {
    return `/api//users/${self.uuid}/avatar?type=thumb&v=0`;
  }
}));


const Activity = types.model("Activity", {
  adminUuid: types.string,
  avatarLargeUrl: types.maybe(types.string),
  avatarThumbUrl: types.maybe(types.string),
  createdAt: types.number,
  creatorUuid: types.string,
  desc: types.maybe(types.string),
  event: Event,
  partecipants: types.optional(types.array(types.string), []),
  polls: types.optional(types.array(Poll), []),
  status: types.number,
  users: types.optional(types.array(User), []),
  usersCanAddItems: types.boolean,
  usersCanInvite: types.boolean,
  uuid: types.string,
  version: types.number,
  whenPollEnabled: types.boolean,
  wherePollEnabled: types.boolean,
  whenPollUuid: types.maybe(types.string),
  wherePollUuid: types.maybe(types.string)
}).views(self => ({
  get creatorAvatarSmall() {
    return `/api//users/${self.creatorUuid}/avatar?type=thumb&v=0`;
  },

  get statusText() {
    switch (self.status) {
      case 1:
        return i18n.t("activity.event.state.planning");
      case 2:
        return i18n.t("activity.event.state.confirmed");
      default:
        return i18n.t("activity.event.state.canceled");
    }
  },

  get statusPratecipanti() {
    switch (self.status) {
      case 1:
        return i18n.t("activity.item.num_interested", {count: self.event.numInteressati});
      case 2:
        return `${self.event.numConfermati}/${self.partecipants.length}`;
      default:
        return "";
    }
  },

  get doveText() {
    return self.wherePollEnabled ? i18n.t("activity.event.poll_in_progress") : self.event.where;
  },

  get quandoText() {
    return self.whenPollEnabled ? i18n.t("activity.event.poll_in_progress") : self.event.fromDate;
  },

  get whoPropose() {
    if (self.creatorUuid === authStore.userUUID) {
      return i18n.t("activity.item.propose.me")
    } else {
      const user = self.users.find(user => user.uuid === self.creatorUuid);
      if (user) {
        return i18n.t("activity.item.propose.other", {who: user.nickname});
      }
      return i18n.t("activity.item.propose.other", {who: "Someone"});
    }
  },

  get whoProposeDetail() {
    if (self.creatorUuid === authStore.userUUID) {
      return "You"
    } else {
      const user = self.users.find(user => user.uuid === self.creatorUuid);
      if (user) {
        return `${user.nickname}`;
      }
      return "Someone";
    }
  },

  get showVoteLayout() {
    switch (self.status) {
      case 1:
        return !self.event.votes.find(vote => vote.userUuid === authStore.userUUID && vote.isInteresse);
      case 2:
        return !self.event.votes.find(vote => vote.userUuid === authStore.userUUID && vote.isConferma);
      default:
        return false;
    }
  },

  get wherePoll() {
    return self.polls.find(poll => poll.isDove);
  },

  get whenPoll() {
    return self.polls.find(poll => poll.isQuando);
  },

  get normalPolls() {
    return self.polls.filter(poll => !poll.isDove && !poll.isQuando);
  },

  get partecipanti() {
    return self.users.filter(user => self.partecipants.indexOf(user.uuid) >= 0)
  },

  get imAdmin() {
    return self.adminUuid === authStore.userUUID;
  },

  get canAddItems() {
    return self.usersCanAddItems || self.imAdmin;
  },

  get canAddUsers() {
    return self.usersCanInvite || self.imAdmin;
  },

  get tabTitles() {
    return [i18n.t("activity.detail.tab.info"), i18n.t("activity.detail.tab.poll"), i18n.t("activity.detail.tab.chat"), i18n.t("activity.detail.tab.user")];
  }
})).actions(self => {
  const refresh = flow(function*() {
    const {ok, data} = yield api.get(`api/activity/${self.uuid}`);
    if (ok) {
      applySnapshot(self, data);
    }
    return {ok, data};
  });

  const voteConferma = flow(function*(isYes) {
    const {ok, data} = yield api.post(`api/activity/${self.uuid}/event/vote`, {
      type: isYes ? 1 : 2
    });
    if (!ok) {
      errorStore.notifyError(data.errorMessage);
    }
  });

  const voteInteresse = flow(function*(isYes) {
    const {ok, data} = yield api.post(`api/activity/${self.uuid}/event/vote`, {
      type: isYes ? 3 : 4
    });
    if (!ok) {
      errorStore.notifyError(data.errorMessage);
    }
  });

  function createPollStore() {
    return CreatePollStore.create({activityUuid: self.uuid});
  }

  function addUserStore() {
    return AddUserStore.create({activityUuid: self.uuid});
  }

  function userById(uuid) {
    return self.users.find(user => user.uuid === uuid);
  }

  return {
    refresh,
    voteInteresse,
    voteConferma,
    createPollStore,
    addUserStore,
    userById
  }
});


export default types
  .model("ModelStore", {
    activities: types.optional(types.array(Activity), []),
    selectedActivity: ""
  })
  .views(self => ({
    getActivityByUuid(uuid) {
      return self.activities.find(activity => activity.uuid === uuid);
    }
  }))
  .actions(self => {
    function selectActivity(uuid) {
      self.selectedActivity = uuid;
    }

    const loadActivities = flow(function*() {
      const {ok, data} = yield api.get("/api/activities");
      if (ok) {
        self.activities = data;
      } else {
        errorStore.notifyError(data.errorMessage);
      }
    });

    const refreshActivity = flow(function*(uuid) {
      const activity = self.activities.find(activity => activity.uuid === uuid);
      if (activity) {
        const {ok, data} = yield activity.refresh();
        if (!ok) {
          errorStore.notifyError(data.errorMessage);
        }
      }
    });

    const loadActivity = flow(function*(uuid) {
      const {ok, data} = yield api.get(`api/activity/${uuid}`);
      if (ok) {
        self.activities.push(data);
      } else {
        errorStore.notifyError(data.errorMessage);
      }
    });

    const afterCreate = () => {
      if (typeof window !== undefined && window.localStorage) {
        const sActivity = window.localStorage.getItem("selectedActivity");
        if (sActivity) {
          applySnapshot(self, JSON.parse(sActivity));
        }

        reaction(
          () => getSnapshot(self),
          json => {
            window.localStorage.setItem("selectedActivity", JSON.stringify(json))
          }
        );
      }
    };

    return {
      selectActivity,
      loadActivities,
      refreshActivity,
      loadActivity,
      afterCreate
    }
  })
  .create({});