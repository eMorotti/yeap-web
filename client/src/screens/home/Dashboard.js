import React from "react";
import {inject, observer} from "mobx-react";
import Image from './../../components/Image';
import {translate} from 'react-i18next';

@translate()
@inject(stores => ({
  store: stores.modelStore,
}))
@observer
class Dashboard extends React.Component {

  render() {
    const {activities} = this.props.store;

    return activities.map((activity) => {
      return this._renderActivity(activity);
    });
  }

  _renderActivity(activity) {
    const {selectedActivity, selectActivity} = this.props.store;
    const {uuid, avatarThumbUrl, event, whoPropose, statusText, statusPratecipanti, doveText, quandoText} = activity;

    let cardStyle = "card small waves-effect";
    if (selectedActivity === uuid) {
      cardStyle += " blue-grey";
    }

    return (
      <div key={uuid} className={cardStyle} style={{display: "block"}}
           onClick={() => selectActivity(uuid)           }>
        <div className="card-content">
          <div className="row">
            <div className="col s2">
              <div className="col s8 offset-s2">
                <Image src={avatarThumbUrl} alt="" className="circle responsive-img"
                       unloader="/images/avatar_attivita.jpg"/>
              </div>
            </div>
            <div className="col s10">
              <span>{whoPropose}</span>
              <span className="card-title">{event.what}</span>
              <div>
                <span className="new badge left">{statusText}</span>
                <span>{statusPratecipanti}</span>
                <p>{doveText}</p>
                <p>{quandoText}</p>
              </div>
            </div>
          </div>
        </div>
        {this._renderVoteLayout(activity)}
      </div>
    );
  }

  _renderVoteLayout(activity) {
    const {showVoteLayout} = activity;

    if (showVoteLayout) {
      return (
        <div className="card-action blue">
          {this._renderVote(activity)}
        </div>
      );
    }
    return null;
  }

  _renderVote({status, voteInteresse, voteConferma}) {
    switch (status) {
      case 1:
        return (
          <div>
            <button onClick={(event) => {
              event.stopPropagation();
              voteInteresse(false);
            }} className="waves-effect waves-light btn">Non interessato
            </button>
            <button onClick={(event) => {
              event.stopPropagation();
              voteInteresse(true);
            }} className="waves-effect waves-light btn right">Interessato
            </button>
          </div>
        );
      case 2:
        return (
          <div>
            <button onClick={(event) => {
              event.stopPropagation();
              voteConferma(false);
            }} className="waves-effect waves-light btn">No!
            </button>
            <button onClick={(event) => {
              event.stopPropagation();
              voteConferma(true);
            }} className="waves-effect waves-light btn right">Yeap!
            </button>
          </div>
        );
      default:
        return null;
    }
  }
}

export default Dashboard;