import React from 'react';
import {inject, observer} from "mobx-react";

import Login from "./Login";
import CreateProfile from "./CreateProfile";

@inject(stores => ({
  store: stores.authStore
}))
@observer
class Home extends React.Component {
  render() {
    return (
      <div className="container">
        {this._renderContent()}
      </div>
    )
  }

  _renderContent() {
    const {token} = this.props.store;

    if (token) {
      return <CreateProfile/>;
    }
    return <Login/>;
  }
}

export default Home;