import React from 'react';
import {inject, observer} from "mobx-react";

@inject(stores => ({
  store: stores.profileStore
}))
@observer
class CreateProfile extends React.Component {
  render() {
    const {fileName, nickname, setNickname, onSubmit} = this.props.store;

    return (
      <div className="row">
        <div className="col s6 offset-s3">
          <div className="row">
            <div className="row">
              <div className="row">
                <div className="col s4 offset-s4">
                  <img src="https://freeiconshop.com/wp-content/uploads/edd/person-flat.png" alt=""
                       className="circle responsive-img"/>
                </div>
              </div>
              <div className="col file-field input-field s6 offset-s3">
                <div className="btn">
                  <span>Choose</span>
                  <input type="file" onChange={(event) => this._handleFileChange(event.target.files)}/>
                </div>
                <div className="file-path-wrapper">
                  <input className="file-path validate" type="text" value={fileName}/>
                </div>
              </div>
            </div>
          </div>
          <label>Nickname</label>
          <input id="nickname" type="text" className="validate" value={nickname}
                 onChange={(event) => setNickname(event.target.value)}/>
          <button className="waves-effect waves-light btn right" onClick={onSubmit}>Submit</button>
        </div>
      </div>
    );
  }

  _handleFileChange(fileList) {
    const {setAvatar} = this.props.store;

    if (fileList && fileList.length) {
      setAvatar(fileList[0]);
    }
  }
}

export default CreateProfile;