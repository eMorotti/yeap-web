import React from 'react';
import {inject, observer} from "mobx-react";

@inject(stores => ({
  store: stores.authStore
}))
@observer
class Login extends React.Component {

  render() {
    const {accountKit_SmsLogin} = this.props.store;

    return (
      <div className="row">
        <div className="col s6 offset-s3">
          <div className="card darken-1">
            <div className="card-content">
              <span className="card-title">Yeap</span>
              <p>SignIn in with AccountKit</p>
            </div>
            <div className="card-action">
              <a href="#login" onClick={accountKit_SmsLogin}>SignIn</a>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Login;