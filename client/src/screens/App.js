import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {inject, observer} from "mobx-react";

import ActivitiesAware from "./components/ActivitiesAware";
import ErrorAware from "./components/ErrorAware";
import ConnectorAware from "./components/ConnectorAware";
import SignIn from './signin/SignIn';
import Dashboard from './home/Dashboard';
import Header from './components/Header';
import ActivityDetail from './activity/ActivityDetail';

@inject(stores => ({
  store: stores.authStore
}))
@observer
class App extends React.Component {

  render() {
    const {isLoggedIn} = this.props.store;

    return (
      <div>
        <BrowserRouter>
          <div>
            <Header/>
            <ErrorAware/>
            <Switch>
              {isLoggedIn ?
                this._renderLoggedIn()
                :
                this._renderGuest()
              }
            </Switch>
          </div>
        </BrowserRouter>
      </div >
    );
  }

  _renderLoggedIn() {
    return (
      <ConnectorAware>
        <ActivitiesAware/>
        <div>
          <Route path="/" render={() => (
            <div className="row">
              <div className="col s3 offset-s2">
                <Dashboard/>
              </div>
              <div className="col s5">
                <ActivityDetail/>
              </div>
            </div>
          )}/>
        </div>
      </ConnectorAware>
    );
  }

  _renderGuest() {
    return (
      <div>
        <Route exact path="/" component={SignIn}/>
        <Route path="/activity/:uuid" render={() => (
          <div>
            <ActivityDetail/>
          </div>
        )}/>
      </div>
    );
  }
}

export default App;
