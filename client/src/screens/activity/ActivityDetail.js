import React from "react";
import {inject, observer} from "mobx-react";
import {withRouter} from "react-router-dom";

import ActivityDetailInfo from "./ActivityDetailInfo";
import ActivityDetailPolls from "./ActivityDetailPolls";
import ActivityDetailMessages from "./ActivityDetailMessages";
import ActivityDetailUsers from "./ActivityDetailUsers";
import Loader from "./../../components/Loader";

@inject(stores => ({
  store: stores.modelStore
}))
@observer
class ActivityDetail extends React.Component {
  state = {
    selectedPage: 0
  };

  constructor(props) {
    super(props);

    const {uuid} = props.match.params;
    const {loadActivity} = props.store;

    if (uuid) {
      loadActivity(uuid);
    }
  }

  componentWillReceiveProps(newProps) {
    const {getActivityByUuid, selectedActivity} = newProps.store;
    const {uuid} = this.props.match.params;
    const activity = getActivityByUuid(uuid || selectedActivity);

    if (activity.uuid !== this.uuid) {
      this.setState({
        selectedPage: 0
      })
    }
  }

  render() {
    const {getActivityByUuid, selectedActivity} = this.props.store;
    const {uuid: id} = this.props.match.params;
    const activity = getActivityByUuid(id || selectedActivity);

    if (!activity) {
      return (
        <div className="container">
          <Loader />
        </div>
      );
    }

    const {uuid, tabTitles} = activity;
    this.uuid = uuid;

    return (
      <div className="row">
        <div>
          <button onClick={() => this._onTabChange(0)}>{tabTitles[0]}</button>
          <button onClick={() => this._onTabChange(1)}>{tabTitles[1]}</button>
          <button onClick={() => this._onTabChange(2)}>{tabTitles[2]}</button>
          <button onClick={() => this._onTabChange(3)}>{tabTitles[3]}</button>
        </div>
        {this.state.selectedPage === 0 && <ActivityDetailInfo activity={activity}/>}
        {this.state.selectedPage === 1 && <ActivityDetailPolls activity={activity}/>}
        {this.state.selectedPage === 2 && <ActivityDetailMessages activity={activity}/>}
        {this.state.selectedPage === 3 && <ActivityDetailUsers activity={activity}/>}
      </div>
    );
  }

  _onTabChange(position) {
    this.setState({
      selectedPage: position
    })
  }
}

export default withRouter(ActivityDetail);