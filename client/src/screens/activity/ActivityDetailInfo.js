import React from "react";
import {observer} from "mobx-react";
import Image from './../../components/Image';

import PollView from "../components/PollView";

@observer
class ActivityDetailInfo extends React.Component {

  render() {
    const {
      avatarLargeUrl, event: {what}, doveText, quandoText, creatorAvatarSmall, whoProposeDetail, desc, whenPollEnabled,
      wherePollEnabled, wherePoll, whenPoll
    } = this.props.activity;

    return (
      <div>
        <div className="row">
          <div className="col s12">
            <div className="card">
              <div className="card-image">
                <Image src={avatarLargeUrl} alt="" unloader="/images/avatar_attivita.jpg"/>
                <span className="card-title">{what}</span>
              </div>
              <div className="card-content">
                <p>{doveText}</p>
                <p>{quandoText}</p>
                <div className="row">
                  <div className="col s2">
                    <Image src={creatorAvatarSmall} alt="" className="circle responsive-img"
                           unloader="/images/avatar_utente.png"/>
                  </div>
                  <p>{whoProposeDetail}</p>
                  <p>{desc}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col s12">
            {this._renderPartecipanti(this.props.activity)}
          </div>
        </div>
        <div className="row">
          {whenPollEnabled ? <PollView poll={whenPoll}/> : null}
        </div>
        <div className="row">
          {wherePollEnabled ? <PollView poll={wherePoll}/> : null}
        </div>
        <div className="row">
          {this._renderVoteLayout(this.props.activity)}
        </div>
      </div>
    );
  }

  _renderPartecipanti(activity) {
    const {status} = activity;

    switch (status) {
      case 1:
      case 2:
        return (
          <div className="card">
            <div className="card-content">
              <div className="row">
                {this._renderUsers(activity)}
              </div>
            </div>
          </div>
        );
      default:
        return null;
    }
  }

  _renderUsers({event, userById}) {
    const {votesTitle, votesYes, votesNo, votesNotVotedText} = event;

    const usersYes = votesYes.map(vote => {
      const user = userById(vote.userUuid);
      if (user) {
        return this._renderUser(user);
      }
      return null;
    });

    const usersNo = votesNo.map(vote => {
      const user = userById(vote.userUuid);
      if (user) {
        return this._renderUser(user);
      }
      return null;
    });

    return (
      <div>
        <p>{votesTitle}</p>
        <div className="row">
          {usersYes}
        </div>
        <div className="row">
          {usersNo}
        </div>
        <p>{votesNotVotedText}</p>
      </div>
    );
  }

  _renderUser(user) {
    const {uuid, avatarSmall} = user;

    return (
      <div key={uuid} className="col s1">
        <Image src={avatarSmall} alt="" className="circle responsive-img"
               unloader="/images/avatar_utente.png"/>
      </div>
    );
  }

  _renderVoteLayout(activity) {
    const {showVoteLayout} = activity;

    if (showVoteLayout) {
      return (
        <div className="card-action blue">
          {this._renderVote(activity)}
        </div>
      );
    }
    return null;
  }

  _renderVote({status, voteInteresse, voteConferma}) {
    switch (status) {
      case 1:
        return (
          <div>
            <button onClick={(event) => {
              event.stopPropagation();
              voteInteresse(false);
            }} className="waves-effect waves-light btn">Non interessato
            </button>
            <button onClick={(event) => {
              event.stopPropagation();
              voteInteresse(true);
            }} className="waves-effect waves-light btn right">Interessato
            </button>
          </div>
        );
      case 2:
        return (
          <div>
            <button onClick={(event) => {
              event.stopPropagation();
              voteConferma(false);
            }} className="waves-effect waves-light btn">No!
            </button>
            <button onClick={(event) => {
              event.stopPropagation();
              voteConferma(true);
            }} className="waves-effect waves-light btn right">Yeap!
            </button>
          </div>
        );
      default:
        return null;
    }
  }
}

export default ActivityDetailInfo;