import React from "react";
import {observer} from "mobx-react";
import {Modal} from 'react-materialize';
import Image from './../../components/Image';
import {translate} from 'react-i18next';

import Loader from "./../../components/Loader";

@translate()
@observer
class ActivityDetailUsers extends React.Component {
  componentWillMount() {
    const {addUserStore} = this.props.activity;
    this.store = addUserStore();
  }

  render() {
    const {t} = this.props;
    const {partecipanti, canAddUsers} = this.props.activity;
    const {phone, setPhone, submitInvite, isLoading} = this.store;

    const users = partecipanti.map(user =>
      this._renderUser(user)
    );

    const addUser =
      <Modal header={t("user.add.popup.title")}
             trigger={<button className="waves-effect waves-light btn">{t("user.add.popup.button")}</button>}
             actions={[
               <button
                 className="waves-effect waves-light btn-flat modal-action modal-close">{t("user.add.popup.cancel_button")}</button>,
               <button className="waves-effect waves-light btn-flat modal-action modal-close"
                       onClick={() => submitInvite()}>{t("user.add.popup.confirm_button")}</button>
             ]}>
        <input id="entry" type="text" className="validate" value={phone}
               onChange={(event) => setPhone(event.target.value)} maxLength="13"/>
      </Modal>;

    return (
      <div>
        <div className="row">
          <div className="col s12">
            {users}
            {isLoading && <Loader/>}
            {canAddUsers && addUser}
          </div>
        </div>
      </div>
    );
  }

  _renderUser(user) {
    const {uuid, avatarSmall, nickname} = user;

    return (
      <div className="card" key={uuid}>
        <div className="card-content">
          <div className="row">
            <div className="col s2">
              <Image src={avatarSmall} alt="" className="circle responsive-img" unloader="/images/avatar_utente.png"/>
            </div>
            <p>{nickname}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default ActivityDetailUsers;