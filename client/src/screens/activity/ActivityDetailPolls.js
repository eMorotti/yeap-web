import React from "react";
import {observer} from "mobx-react";
import {Modal} from 'react-materialize';
import {translate} from 'react-i18next';

import PollView from "../components/PollView";
import Loader from "./../../components/Loader";

@translate()
@observer
class ActivityDetailPolls extends React.Component {
  componentWillMount() {
    const {createPollStore} = this.props.activity;
    this.store = createPollStore();
  }

  render() {
    const {t} = this.props;
    const {normalPolls, canAddItems} = this.props.activity;
    const {question, setQuestion, entryList, submitPoll, isLoading} = this.store;

    const polls = normalPolls.map(poll =>
      <PollView key={poll.uuid} poll={poll}/>
    );

    const addPolls =
      <Modal header={t("poll.add.popup.title")} trigger={<button className="waves-effect waves-light btn">{t("poll.add.popup.button")}</button>}
             actions={[
               <button className="waves-effect waves-light btn-flat modal-action modal-close">{t("poll.add.popup.cancel_button")}</button>,
               <button className="waves-effect waves-light btn-flat modal-action modal-close"
                       onClick={() => submitPoll()}>{t("poll.add.popup.confirm_button")}</button>
             ]}>
        <input id="entry" type="text" className="validate" value={question}
               onChange={(event) => setQuestion(event.target.value)}/>
        {this._renderModalEntryList(entryList)}
      </Modal>;

    return (
      <div>
        <div className="row">
          {polls}
          {isLoading && <Loader/>}
          {canAddItems && addPolls}
        </div>
      </div>
    );
  }

  _renderModalEntryList(entryList) {
    const {getEntryFromList, setEntryInList} = this.store;
    let maxIndex = -1;

    const entries = entryList.map((value, index) => {
      maxIndex = index;
      return <input key={index} id={index} type="text" className="validate" value={value}
                    onChange={(event) => setEntryInList(index, event.target.value)}/>
    });

    maxIndex++;
    entries.push(<input key={maxIndex} id={maxIndex} type="text" className="validate" value={getEntryFromList(maxIndex)}
                        onChange={(event) => setEntryInList(maxIndex, event.target.value)}/>);

    return entries;
  }
}

export default ActivityDetailPolls;