import React from "react";
import {inject, observer} from "mobx-react";

@inject(stores => ({
  store: stores.modelStore
}))
@observer
class ActivitiesAware extends React.Component {

  componentDidMount() {
    const {loadActivities} = this.props.store;
    loadActivities();
  }

  render() {
    return (
      <div/>
    );
  }
}

export default ActivitiesAware;