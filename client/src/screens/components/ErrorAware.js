import React from "react";
import {inject, observer} from "mobx-react";
import {reaction} from "mobx";
import Materialize from "materialize-css/dist/js/materialize.js";

@inject(stores => ({
  store: stores.errorStore
}))
@observer
class ErrorAware extends React.Component {

  componentDidMount() {
    reaction(
      () => this.props.store.error,
      (value) => {
        if (value) {
          Materialize.toast(value, 4000);
          this.props.store.errorShown();
        }
      }
    )
  }

  render() {
    return (
      <div/>
    );
  }
}

export default ErrorAware;