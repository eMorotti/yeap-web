import React from "react";
import {inject, observer} from "mobx-react";
import {translate} from 'react-i18next';
import {withRouter} from "react-router-dom";
import {Link} from "react-router-dom";
import {Row, Input} from 'react-materialize';
import i18n from 'i18next';

@translate()
@inject(stores => ({
  store: stores.authStore
}))
@observer
class Header extends React.Component {
  componentDidMount() {
    // const {history} = this.props;
    //
    // i18n.on("languageChanged", (lng) => {
    //   history.replace({
    //     pathname: history.location.pathname,
    //     search: "?lng=" + lng
    //   })
    // });
  }

  render() {
    const {t} = this.props;
    const {logout, isLoggedIn} = this.props.store;

    const logoutView = <li key="logout"><a href="/" onClick={() => logout()}>{t("auth.logout.button")}</a></li>;

    return (
      <div className="navbar-fixed">
        <nav>
          <div className="nav-wrapper container">
            <Link to="/" className="brand-logo">Yeap!</Link>
            <ul id="nav-mobile" className="right hide-on-med-and-down">
              <li>
                <a>
                  <Row style={{paddingTop: "6px"}}>
                    <Input type='select' defaultValue={i18n.language}
                           onChange={event => i18n.changeLanguage(event.target.value)}>
                      <option value='en'>EN</option>
                      <option value='it'>IT</option>
                    </Input>
                  </Row>
                </a>
              </li>
              {isLoggedIn && logoutView}
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default withRouter(Header);