import React from "react";
import {inject} from "mobx-react";

@inject(stores => ({
  store: stores.connectorStore
}))
class ConnectorAware extends React.Component {

  componentDidMount() {
    this.props.store.start();
  }

  componentWillUnmount() {
    this.props.store.stop();
  }

  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

export default ConnectorAware;