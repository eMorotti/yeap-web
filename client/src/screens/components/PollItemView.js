import React from "react";
import {observer} from "mobx-react";

import Loader from "./../../components/Loader";

@observer
class PollItemView extends React.Component {
  render() {
    const {voce, mioVoto, vote, isLoading, canRemoveItem, removeItem} = this.props.pollitem;

    const loader = <Loader style={{width: "15px", height: "15px"}}/>;
    const remove = <button onClick={removeItem}>REMOVE</button>;

    return (
      <div onClick={() => vote()}>
        <input type="checkbox" className="filled-in" checked={mioVoto !== undefined} onChange={() => {
        }}/>
        <label>{voce}</label>
        {isLoading && loader}
        {canRemoveItem && remove}
      </div>
    );
  }
}

export default PollItemView;