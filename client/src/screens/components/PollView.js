import React from "react";
import {observer} from "mobx-react";
import {Modal} from 'react-materialize';
import PlacesAutocomplete from 'react-places-autocomplete';
import {translate} from 'react-i18next';

import PollItemView from "./PollItemView";
import Loader from "./../../components/Loader";

@translate()
@observer
class PollView extends React.Component {
  componentWillMount() {
    const {createPollItemStore} = this.props.poll;
    this.store = createPollItemStore();
  }

  render() {
    const {t} = this.props;
    const {question, pollItemsActive, canAddItems, isDove, isQuando} = this.props.poll;
    const {
      entry, setEntry, submitPollItem, fromDate, setFromDate, fromHour, setFromHour, toDate, setToDate,
      toHour, setToHour, where, setWhere, setPlace, isLoading
    } = this.store;

    const pollItemList = pollItemsActive.map(pollItem => {
      return <PollItemView key={pollItem.uuid} pollitem={pollItem}/>
    });

    const addEntry = !isDove && !isQuando ?
      <Modal header={t("poll.item.add.popup.title")} trigger={<button className="waves-effect waves-light btn">{t("poll.item.add.popup.button")}</button>}
             actions={[
               <button className="waves-effect waves-light btn-flat modal-action modal-close">{t("poll.item.add.popup.cancel_button")}</button>,
               <button className="waves-effect waves-light btn-flat modal-action modal-close"
                       onClick={() => submitPollItem()}>{t("poll.item.add.popup.confirm_button")}</button>
             ]}>
        <input id="entry" type="text" className="validate" value={entry}
               onChange={(event) => setEntry(event.target.value)}/>
      </Modal> : null;

    const addEntryWhen = isQuando ?
      <Modal header={t("poll.item.add.popup_when.title")} trigger={<button className="waves-effect waves-light btn">{t("poll.item.add.popup_when.button")}</button>}
             actions={[
               <button className="waves-effect waves-light btn-flat modal-action modal-close">{t("poll.item.add.popup_when.cancel_button")}</button>,
               <button className="waves-effect waves-light btn-flat modal-action modal-close"
                       onClick={() => submitPollItem()}>{t("poll.item.add.popup_when.confirm_button")}</button>
             ]}>
        <input id="entry" type="text" className="validate" value={fromDate} placeholder="dd/mm/yyyy"
               onChange={(event) => setFromDate(event.target.value)} maxLength="10"/>
        <input id="entry" type="text" className="validate" value={fromHour} placeholder="hh:mm"
               onChange={(event) => setFromHour(event.target.value)} maxLength="5"/>
        <input id="entry" type="text" className="validate" value={toDate} placeholder="dd/mm/yyyy"
               onChange={(event) => setToDate(event.target.value)} maxLength="10"/>
        <input id="entry" type="text" className="validate" value={toHour} placeholder="hh:mm"
               onChange={(event) => setToHour(event.target.value)} maxLength="5"/>
      </Modal> : null;

    const addEntryWhere = isDove ?
      <Modal header={t("poll.item.add.popup_where.title")} trigger={<button className="waves-effect waves-light btn">{t("poll.item.add.popup_where.button")}</button>}
             actions={[
               <button className="waves-effect waves-light btn-flat modal-action modal-close">{t("poll.item.add.popup_where.cancel_button")}</button>,
               <button className="waves-effect waves-light btn-flat modal-action modal-close"
                       onClick={() => submitPollItem()}>{t("poll.item.add.popup_where.confirm_button")}</button>
             ]}>
        <PlacesAutocomplete inputProps={{value: where, onChange: (event) => setWhere(event)}}
                            onSelect={setPlace}
                            onError={() => {
                            }}/>
      </Modal> : null;

    return (
      <div className="col s12">
        <div className="card">
          <div className="card-content">
            <span className="card-title">{question}</span>
            {pollItemList}
            {isLoading && <Loader/>}
            {canAddItems && (addEntry || addEntryWhen || addEntryWhere)}
          </div>
        </div>
      </div>
    );
  }
}

export default PollView;