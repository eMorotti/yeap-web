import "materialize-css/dist/css/materialize.min.css";
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'mobx-react';
import {I18nextProvider} from 'react-i18next';

import App from './screens/App';
import * as stores from "./stores";
import registerServiceWorker from './registerServiceWorker';
import i18n from './i18n';

ReactDOM.render(
  <I18nextProvider i18n={i18n}>
    <Provider {...stores}>
      <App />
    </Provider>
  </I18nextProvider>,
  document.getElementById('root')
);
registerServiceWorker();
