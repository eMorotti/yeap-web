import React from "react";

export default (props) => {
  const newProps = {};
  if(props.style) {
    if(props.style.width) {
      newProps.width = props.style.width;
    }
    if(props.style.height) {
      newProps.height = props.style.height;
    }
  }

  return (
    <div className="preloader-wrapper big active" style={newProps}>
      <div className="spinner-layer spinner-green-only">
        <div className="circle-clipper left">
          <div className="circle"></div>
        </div>
        <div className="gap-patch">
          <div className="circle"></div>
        </div>
        <div className="circle-clipper right">
          <div className="circle"></div>
        </div>
      </div>
    </div>
  );
}