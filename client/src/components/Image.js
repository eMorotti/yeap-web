import React from "react";
import Img from 'react-image';

export default (props) => {
  return <Img src={props.src} alt="" className={props.className}
              loader={<img src={props.unloader} alt="" className={props.className}/>}
              unloader={<img src={props.unloader} alt="" className={props.className}/>}/>
}